require("./lib/jquery");

$(document).ready(function () {
	var Api = require("./api");
	var I18 = require("./lang/I18").Class;
	var Chrome = require("./ui/chrome").Class;

	var i18 = new I18();
	i18.setLanguage("english");
	var timer = -1;
	var ch = new Chrome(Api, i18);
	$("body").first().append(ch.el);
	ch.resize();
	$(window).bind("resize", function(e){
		ch.showResizeBox(true);
		clearTimeout(timer);
		timer = setTimeout(function() {
			ch.resize();
			ch.showResizeBox(false);
		}, 250);
	});
});