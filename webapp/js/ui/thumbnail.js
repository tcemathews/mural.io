require("../lib/jquery");

function Thumbnail () {
	this.data = null;
	this.el = $("<div></div>");
	this.el.addClass("thumbnail");
	this.img = $("<img/>");
	this.el.append(this.img);
	this.el.bind("click", (this.onClick).bind(this));
	this.timer = null;
}

Thumbnail.prototype.setImage = function ( data ) {
	this.el.append("<div class=\"board spinner\"></div>");
	this.img.css("opacity", "0");
	this.img.attr("src", "/thumbnail/" + data.filename);
	this.img.on("load", (this.onLoad_Image).bind(this));
	this.data = data;
}

Thumbnail.prototype.fadeIn = function ( delay ) {
	if ( this.timer ) {
		clearTimeout(this.timer);
	}
	this.timer = setTimeout((this.onTimeout_FadeIn).bind(this), delay);
}

// Event Methods

Thumbnail.prototype.onTimeout_FadeIn = function ( e ) {
	this.el.find("div.board.spinner").remove();
	this.img.animate({opacity:1}, {duration:1000});
};

Thumbnail.prototype.onLoad_Image = function ( e ) {
	this.fadeIn(1000);
};

Thumbnail.prototype.onClick = function ( e ) {
	this.el.trigger("activated", this);
};

module.exports.Class = Thumbnail;