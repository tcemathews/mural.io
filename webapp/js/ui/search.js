require("../lib/jquery");

function Search(api) {
	this.api = api;
	this.defaultSuggestions = [];

	this.el = $("<div></div>");
	this.el.addClass("header");

	this.container = $("<div>");
	this.container.addClass("search");
	this.el.append(this.container);

	this.tags = $("<div>");
	this.tags.addClass("tags");
	this.tags.html("<ul></ul>");
	this.el.append(this.tags);

	this.tagsList = this.tags.find("ul").first();

	this.input = $("<input>");
	this.input.attr("spellcheck", "false");
	this.input.attr("placeholder", "fresh");
	this.container.append(this.input);
	this.input.keyup((this.onKeyup_Input).bind(this));
	this.input.bind("focusin", (this.onFocusIn_Input).bind(this));
	this.input.bind("focusout", (this.onFocusOut_Input).bind(this));

	this.showSuggestionBox(false);
	this.restoreDefaultSuggestions();
	this.updateDefaultSuggestions();
}

Search.prototype.updateDefaultSuggestions = function () {
	this.api.getPopularTags((this.onApi_GetPopularTags).bind(this));
}

Search.prototype.restoreDefaultSuggestions = function () {
	this.setTagSuggestions(this.defaultSuggestions);
}

Search.prototype.submitSearch = function () {
	this.showSuggestionBox(false);
	this.input.blur();
	this.el.trigger("submit", this.input.val());
}

Search.prototype.hasSuggestions = function () {
	return this.tagsList.children("li").length > 0;
}

Search.prototype.isDisplayingSuggestionBox = function () {
	return this.tags.css("display") == "block";
}

Search.prototype.showSuggestionBox = function ( value ) {
	var display = value ? "block" : "none";
	this.tags.css("display", display);
}

Search.prototype.setTagSuggestions = function ( arr ) {
	var list = this.tagsList;
	list.empty();
	for ( var i in arr ) {
		var item = $("<li>");
		item.html(arr[i]);
		item.attr("data-suggestion", arr[i]);
		item.bind("click", (this.onClick_SuggestionItem).bind(this));	
		list.append(item);
	}
}

Search.prototype.isAtFront = function () {
	return this.el.css("zIndex") == 101;
}

Search.prototype.bringToFront = function () {
	this.el.css("zIndex", 101);
}

Search.prototype.sendBack = function () {
	this.el.css("zIndex", "");
}

// Event Methods

Search.prototype.onApi_GetPopularTags = function ( response ) {
	if ( response.status == 0 ) {
		var arr = [];
		for ( var i in response.data ) {
			var info = response.data[i];
			arr.push(info.name);
		}
		this.defaultSuggestions = arr;
	}
}

Search.prototype.onClick_SuggestionItem = function ( e ) {
	var suggestion = $(e.target).attr("data-suggestion");
	var tags = this.input.val().trim().split(",");
	var pass = true;

	for ( var i in tags ) {
		if ( tags[i].trim() == suggestion ) {
			pass = false;
		}
	}

	if ( pass ) {
		suggestion = tags.length > 0 && tags[0] != "" ? ", " + suggestion : suggestion;
		this.input.val(this.input.val() + suggestion);
		this.blockNextFocusIn = true;
		this.input.focus();
	}
}

Search.prototype.onFocusIn_Input = function ( e ) {
	if ( !this.blockNextFocusIn ) {
		this.el.trigger("requestDisplaySuggestionBox");	
	}
	this.blockNextFocusIn = false;
}

Search.prototype.onFocusOut_Input = function ( e ) {
	this.el.trigger("requestHideSuggestionBox");
}

Search.prototype.onKeyup_Input = function ( e ) {
	// On enter pressed, submit the query.
	if ( e.keyCode == 13 ) {
		this.submitSearch();
	}
}

module.exports.Class = Search;