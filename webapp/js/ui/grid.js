require("../lib/jquery");
var Thumbnail = require("./thumbnail").Class;

function Grid (i18) {
	this.cache = {};
	this.order = [];
	this.el = $("<div></div>");
	this.el.addClass("grid");
	this.container = $("<div></div>");
	this.container.addClass("thumbnails");
	this.buttonMore = $("<button>");
	this.buttonMore.addClass("flat");
	this.buttonMore.html(i18.strings["continue-searching"]);
	this.containerMore = $("<div></div>");
	this.containerMore.addClass("more");
	this.containerMore.append(this.buttonMore);
	this.labelNoResults = $("<label></label>");
	this.labelNoResults.addClass("no-results");
	this.labelNoResults.html(i18.strings["no-results"]);
	this.el.append(this.container);
	this.el.append(this.containerMore);
	this.el.append(this.labelNoResults);
	this.buttonMore.bind("click", (this.onClick_MoreButton).bind(this));
	this.showMoreButton(false);
}

Grid.prototype.resize = function () {
	var pusher = this.el.parent().find(".header").first();
	var parent = this.el.parent();
	this.el.width(parent.width());
	this.el.height(parent.height() - pusher.height());
}

Grid.prototype.clear = function () {
	this.cache = {};
	this.order = [];
	this.el.empty();
	this.container.empty();
	this.el.append(this.container);
	this.el.append(this.containerMore);
	this.el.append(this.labelNoResults);
	this.buttonMore.bind("click", (this.onClick_MoreButton).bind(this));
	this.showMoreButton(false);
}

Grid.prototype.addImages = function ( arr ) {
	for ( var i in arr ) {
		var d = arr[i];
		if ( !this.cache[d.filename] ) {
			var thumbnail = new Thumbnail();
			thumbnail.setImage(d);
			thumbnail.el.bind("activated", (this.onActivated_Thumbnail).bind(this));
			this.container.append(thumbnail.el);
			this.cache[d.filename] = thumbnail;
			this.order.unshift(d.filename);
		}
	}
}

Grid.prototype.showMoreButton = function ( value ) {
	if ( value ) {
		this.containerMore.show();
	}
	else {
		this.containerMore.hide();
	}
}

Grid.prototype.hasResults = function () {
	return this.order.length > 0;
}

Grid.prototype.showNoResultsLabel = function ( value ) {
	var display = value ? "block" : "none";
	this.labelNoResults.css("display", display);
}

Grid.prototype.getPrevious = function ( data ) {
	for ( var i = 0; i < this.order.length - 1; i++ ) {
		if ( this.order[i] == data.filename ) {
			return this.cache[this.order[i + 1]].data;
		}
	}
	return null;
}

Grid.prototype.getNext = function ( data ) {
	for ( var i = 1; i < this.order.length; i++ ) {
		if ( this.order[i] == data.filename ) {
			return this.cache[this.order[i - 1]].data;
		}
	}
	return null;
}

// Event Methods

Grid.prototype.onClick_MoreButton = function ( e ) {
	this.el.trigger("requestMore");
}

Grid.prototype.onActivated_Thumbnail = function ( e, thumbnail ) {
	this.el.trigger("requestDisplay", thumbnail.data);
}

module.exports.Class = Grid;