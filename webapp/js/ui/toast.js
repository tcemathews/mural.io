require("../lib/jquery");

function Toast() {
	this.timer = -1;
	this.el = $("<div></div>");
	this.wrapper = $("<div></div>");
	this.label = $("<label></label>");
	this.el.addClass("toast");
	this.wrapper.append(this.label);
	this.el.append(this.wrapper);
}

Toast.prototype.show = function ( message, displayTime ) {
	clearTimeout(this.timer);
	this.el.stop();
	this.el.css("opacity", 1);
	this.label.html(message);
	this.timer = setTimeout((this.onTimeUp_Timer).bind(this), displayTime);
}

Toast.prototype.isVisible = function () {
	return this.el.css("opacity") > 0;
}

// Event Methods

Toast.prototype.onTimeUp_Timer = function () {
	clearTimeout(this.timer);
	this.timer = -1;
	this.el.animate({opacity: 0}, {duration:1000, complete:(this.onComplete_Fade).bind(this)});
}

Toast.prototype.onComplete_Fade = function ( e ) {
	this.el.trigger("completed", this);
}

module.exports.Class = Toast;