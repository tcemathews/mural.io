require("../lib/jquery");

function Display(api, i18) {
	this.api = api;
	this.keyupListener = null;
	this.clickListenerClose = null;
	this.clickListenerTags = null;
	this.tagsCache = {};
	
	this.el = $("<div></div>");
	this.el.addClass("display");

	this.container = $("<div></div>");
	this.container.addClass("container");
	this.el.append(this.container);
	
	this.toolbar = $("<div></div>");
	this.toolbar.addClass("toolbar");
	
	this.buttonClose = $("<button></button>");
	this.buttonClose.addClass("close");
	this.buttonClose.html(i18.strings["close"]);
	
	this.buttonDownload = $("<button></button>");
	this.buttonDownload.addClass("download");
	this.buttonDownload.html("<a href=\"\" download>" + i18.strings["download"] + "</a>");

	this.buttonTaglist = $("<button>");
	this.buttonTaglist.addClass("taglist");
	this.buttonTaglist.html(i18.strings["tag-list"]);
	
	this.toolbar.append(this.buttonTaglist);
	this.toolbar.append(this.buttonDownload);
	this.toolbar.append(this.buttonClose);
	
	this.frame = $("<div></div>");
	this.frame.addClass("frame");
	
	this.inset = $("<div></div>");
	this.inset.addClass("inset");
	this.frame.append(this.inset);

	this.img = $("<img/>");
	this.inset.append(this.img);

	this.container.append(this.frame);
	this.container.append(this.toolbar);
}

Display.prototype.show = function ( data ) {
	if ( !data || this.data && this.data.filename == data.filename ) {
		return;
	}
	else if ( this.data ) {
		this.img.stop();
		this.img.unbind("load");
	}
	
	this.data = data;

	var source = "/image/" + data.filename;

	this.buttonDownload.find("a").first().attr("href", source);

	this.img.attr("src", source);
	this.img.bind("load", (this.onLoad_Image).bind(this));
	this.img.css("opacity", 0);

	this.showLoadingSpinner();
	
	if ( !this.clickListenerClose ) {
		this.clickListenerClose = (this.onClick_CloseBtn).bind(this);
		this.buttonClose.bind("click", this.clickListenerClose);
	}

	if ( !this.clickListenerTags ) {
		this.clickListenerTags = (this.onClick_TaglistBtn).bind(this);
		this.buttonTaglist.bind("click", this.clickListenerTags);
	}
	
	if ( !this.keyupListener ) {
		this.keyupListener = (this.onKeyUp_Document).bind(this);
		$(document).bind("keyup", this.keyupListener);
	}

	this.resize();
	this.retrieveTags();
}

Display.prototype.close = function () {
	if ( this.keyupListener ) {
		$(document).unbind("keyup", this.keyupListener);
		this.keyupListener = null;
	}
	this.clickListenerTags = null;
	this.clickListenerClose = null;
	this.data = null;
	this.img.stop();
	this.img.unbind("load");
	this.hideLoadingSpinner();
	this.el.trigger("requestRemoval", this);
}

Display.prototype.showLoadingSpinner = function () {
	this.inset.append("<div class=\"board spinner\"></div>");
}

Display.prototype.hideLoadingSpinner = function () {
	this.inset.find("div.board.spinner").remove();
}

Display.prototype.isShowing = function () {
	return this.data != null;
}

Display.prototype.resize = function () {
	var parent = this.container.parent();
	this.container.width(parent.width());
	this.container.height(parent.height());
	this.fitImageToFrame();
}

Display.prototype.fitImageToFrame = function () {
	if ( this.data ) {
		var fw = this.inset.width();
		var fh = this.inset.height();
		var iw = this.img.width();
		var ih = this.img.height();

		this.img.css("marginTop", ((fh - ih) / 2) + "px");
		this.img.css("marginLeft", ((fw - iw) / 2) + "px");
	}
}

Display.prototype.clearTagsCache = function () {
	this.tagsCache = {};
}

Display.prototype.getTags = function () {
	return this.tagsCache[this.data.id] || [];
}

Display.prototype.retrieveTags = function () {
	if ( !this.tagsCache[this.data.id] ) {
		var self = this;
		var id = this.data.id;
		this.api.getImageTags(function ( response ) {
			if ( response.status == 0 ) {
				var arr = [];
				for ( var i in response.data ) {
					arr.push(response.data[i].name);
				}
				self.tagsCache[id] = arr;
				self.el.trigger("updateSuggestions", this);
			}
		}, id);
	}
}

// Event Methods

Display.prototype.onLoad_Image = function ( e ) {
	this.hideLoadingSpinner();
	this.fitImageToFrame();
	this.img.animate({opacity: 1}, {duration: 1000});
}

Display.prototype.onClick_CloseBtn = function ( e ) {
	this.close();
}

Display.prototype.onClick_TaglistBtn = function ( e ) {
	this.el.trigger("requestSuggestionBoxOverlay", this);
}

Display.prototype.onKeyUp_Document = function ( e ) {
	// On right key request next.
	if ( e.keyCode == 39 || e.keyCode == 40 ) {
		this.el.trigger("requestNext", this);
	}
	// On left key request previous.
	else if ( e.keyCode == 37 || e.keyCode == 38) {
		this.el.trigger("requestPrevious", this);
	}
	// On escape key close this.
	else if ( e.keyCode == 27 ) {
		this.close();
	}
}

module.exports.Class = Display;