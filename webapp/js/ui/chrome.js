require("../lib/jquery");

var Search = require("./search").Class;
var Grid = require("./grid").Class;
var Toast = require("./toast").Class;
var Display = require("./display").Class;
var Editor = require("./editor").Class;
var Templater = require("./../util/Templater").Class;

function Chrome(api, i18) {
	this.api = api;
	this.i18 = i18;
	this.templater = new Templater();

	this.offset = 0;
	this.limit = 10;
	this.searchQuery = "";
	this.toastsCache = [];

	this.resizeBox = $("<div></div>");
	this.resizeBox.addClass("resizebox");
	
	this.search = new Search(this.api);
	this.search.el.bind("submit", (this.onSubmit_Search).bind(this));
	this.search.el.bind("requestDisplaySuggestionBox", (this.onRequestDisplaySuggestionBox_Search).bind(this));
	
	this.grid = new Grid(this.i18);
	this.grid.el.bind("requestMore", (this.onRequestMore_Grid).bind(this));
	this.grid.el.bind("requestDisplay", (this.onRequestDisplay_Grid).bind(this));

	this.display = new Display(this.api, this.i18);

	this.toasts = $("<div></div>");
	this.toasts.addClass("toasts");

	this.el = $("<div></div>");
	this.el.addClass("chrome");
	this.el.append(this.search.el);
	this.el.append(this.grid.el);
	this.el.append(this.toasts);
	this.el.append(this.resizeBox);

	this.el.bind("click", (this.onClick_El).bind(this));
	this.el.bind("remove", (this.onRemove_El).bind(this));
	
	this.keyupListener = (this.onKeyUp_Document).bind(this);
	$(document).bind("keyup", this.keyupListener);

	this.commitSearchQuery();
}

// Methods

Chrome.prototype.resize = function () {
	var parent = this.el.parent();
	this.el.width(parent.width());
	this.el.height(parent.height());
	this.display.resize();
	this.grid.resize();
}

Chrome.prototype.addWallpaperUploader = function () {
	var e = new Editor(this.api, this.i18, this.templater);
	this.grid.el.prepend(e.el);
}

Chrome.prototype.showResizeBox = function ( value ) {
	var display = value ? "block" : "none";
	this.resizeBox.css("display", display);
}

Chrome.prototype.setSearchQuery = function ( query ) {
	// If the recent search has change reset the grid view.
	if ( this.searchQuery != query ) {
		this.grid.clear();
		this.searchQuery = query;
		this.offset = 0;
	}
}

Chrome.prototype.commitSearchQuery = function () {
	this.grid.showNoResultsLabel(false);
	this.display.close();

	// Based on the response lets get some new data to show to the user.
	if ( this.searchQuery == "" ) {
		this.api.getLatestImages((this.onApi_GetImages).bind(this), this.offset, this.limit);
	}
	else {
		var included = this.searchQuery.split(", ");
		this.api.getTaggedImages((this.onApi_GetImages).bind(this), included, [], this.offset, this.limit);
	}
}

Chrome.prototype.updateResultsGrid = function ( data ) {
	this.grid.addImages(data);
	this.offset += data.length;

	// Hide the button if no more data and display a toast notification of no more results.
	if ( data.length < this.limit ) {
		this.showToast(this.i18.strings["no-more-results"]);
		this.grid.showMoreButton(false);
		this.grid.showNoResultsLabel(!this.grid.hasResults());
	}
	else {
		this.grid.showMoreButton(true);
	}
}

Chrome.prototype.showToast = function ( message ) {
	var toast = new Toast();
	toast.el.bind("completed", (this.onCompleted_Toast).bind(this));
	this.toasts.append(toast.el);
	toast.show(message, 2000);
	this.toastsCache.push(toast);
}

Chrome.prototype.cleanUpToasts = function () {
	var count = this.toastsCache.length;
	var clean = true;

	// Go through and check if any are visible we don't want to clean up if there are.
	for ( var i = 0; i < count; i++ ) {
		var toast = this.toastsCache[i];
		if ( toast.isVisible() ) {
			clean = false;
		}
	}

	// Empty the displaying toasts and the array.	
	if ( clean ) {
		for ( var i = 0; i < count; i++ ) {
			this.toastsCache[i].el.remove();
		}
		this.toastsCache.length = 0;
	}
}

// Event Methods

Chrome.prototype.onKeyUp_Document = function ( e ) {
	if ( e.ctrlKey && e.keyCode == 221 ) {
		this.addWallpaperUploader();
		//this.toggleEditor();
	}
};

Chrome.prototype.onClick_El = function ( e ) {
	if ( !this.display.isShowing() && this.search.isDisplayingSuggestionBox() && $(e.target).parents().index($(".header")) == -1 ) {
		this.search.showSuggestionBox(false);
	}
};

Chrome.prototype.onRemove_El = function ( e ) {
	$(document).unbind("keyup", this.keyupListener);
};

Chrome.prototype.onCompleted_Toast = function ( e, toast ) {
	this.cleanUpToasts();
};

Chrome.prototype.onSubmit_Search = function ( e, value ) {
	this.setSearchQuery(value);
	this.commitSearchQuery();
};

Chrome.prototype.onRequestDisplaySuggestionBox_Search = function ( e ) {
	// If the display is not showing lets reset our defaults.
	if ( !this.search.isAtFront() ) {
		this.search.restoreDefaultSuggestions();
	}
	this.search.showSuggestionBox(this.search.hasSuggestions());
};

Chrome.prototype.onRequestMore_Grid = function ( e ) {
	this.commitSearchQuery();
};

Chrome.prototype.onRequestDisplay_Grid = function ( e, data ) {
	this.el.append(this.display.el);
	this.display.show(data);
	this.display.el.bind("requestRemoval", (this.onRequestRemoval_Display).bind(this));
	this.display.el.bind("requestNext", (this.onRequestNext_Display).bind(this));
	this.display.el.bind("requestPrevious", (this.onRequestPrevious_Display).bind(this));
	this.display.el.bind("requestSuggestionBoxOverlay", (this.onRequestSuggestionBoxOverlay_Display).bind(this));
	this.display.el.bind("updateSuggestions", (this.onUpdateSuggestions_Display).bind(this));
};

Chrome.prototype.onRequestNext_Display = function ( e, display ) {
	this.search.restoreDefaultSuggestions();
	this.search.showSuggestionBox(false);
	this.search.sendBack();
	display.show(this.grid.getNext(display.data));
}

Chrome.prototype.onRequestPrevious_Display = function ( e, display ) {
	this.search.restoreDefaultSuggestions();
	this.search.showSuggestionBox(false);
	this.search.sendBack();
	display.show(this.grid.getPrevious(display.data));
}

Chrome.prototype.onRequestRemoval_Display = function ( e, display ) {
	display.el.remove();
	this.search.restoreDefaultSuggestions();
	this.search.showSuggestionBox(false);
	this.search.sendBack();
};

Chrome.prototype.onRequestSuggestionBoxOverlay_Display = function ( e, display ) {
	if ( this.search.isAtFront() ) {
		this.search.restoreDefaultSuggestions();
		this.search.showSuggestionBox(false);
		this.search.sendBack();
	}
	else {
		this.search.bringToFront();
		this.search.setTagSuggestions(display.getTags());
		this.search.showSuggestionBox(true);
	}
}

Chrome.prototype.onUpdateSuggestions_Display = function ( e, suggestions ) {
	if ( this.display.isShowing() && this.search.isAtFront() ) {
		this.search.setTagSuggestions(suggestions);
	}
}

Chrome.prototype.onApi_GetImages = function ( response ) {
	// TODO Move this to the grid?
	if ( response.status == 0 ) {
		this.updateResultsGrid(response.data);
	}
	else {
		this.showToast(this.i18.strings["problem-getting-results"]);
	}
};

module.exports.Class = Chrome;