require("../lib/jquery");

var lastPasswordUsed = "";

function Editor ( api, i18, templater ) {
	this.api = api;
	this.i18 = i18;
	this.templater = templater;
	this.disabled = false;
	this.finished = false;

	this.el = $("<div>");
	this.el.addClass("editor");

	this.templater.load("/asset/template/editor.tmpl", (this.onTemplateLoad_Editor).bind(this));
}

Editor.prototype.bindListeners = function () {
	this.submitButton = this.el.find("button.confirm").first();
	this.imageFrame = this.el.find(".image-frame").first();
	this.fileInput = this.el.find("input[type=\"file\"]").first();
	this.tagsInput = this.el.find("input.tags").first();
	this.cancelButton = this.el.find("button.cancel").first();

	this.imageFrame.bind("click", (this.onClick_ImageFrame).bind(this));
	this.submitButton.bind("click", (this.onClick_SubmitButton).bind(this));
	this.cancelButton.bind("click", (this.onClick_CancelButton).bind(this));
	this.fileInput.bind("change", (this.onChange_FileInput).bind(this));
};

Editor.prototype.getTags = function () {
	var tags = this.tagsInput.val().trim().split(",");

	for ( var i in tags ) {
		tags[i] = tags[i].trim();
	}

	return tags;
};

Editor.prototype.setWallpaperSelected = function ( dataUrl ) {
	var img = $("<img/>");
	img.attr("src", dataUrl);
	this.imageFrame.empty();
	this.imageFrame.append(img);
};

Editor.prototype.disable = function () {
	this.disabled = true;
	this.el.addClass("disabled");
	this.submitButton.attr("disabled", true);
	this.submitButton.addClass("disabled");
	this.tagsInput.attr("disabled", true);
	this.tagsInput.addClass("disabled");
	this.imageFrame.addClass("disabled");
	this.imageFrame.append("<div class=\"board spinner\"></div>");
};

Editor.prototype.enable = function () {
	this.disabled = false;
	this.el.removeClass("disabled");
	this.submitButton.removeAttr("disabled");
	this.submitButton.removeClass("disabled");
	this.tagsInput.removeAttr("disabled");
	this.tagsInput.removeClass("disabled");
	this.imageFrame.removeClass("disabled");
	this.imageFrame.find("div.board.spinner").remove();
};

Editor.prototype.makeFinishedState = function () {
	this.finished = true;
}

Editor.prototype.submit = function () {
	var pass = prompt(this.i18.strings["editor"]["request-password"], lastPasswordUsed);
	if ( pass ) {
		this.disable();
		lastPasswordUsed = pass;
		this.api.addImage((this.onApi_AddImage).bind(this), pass, this.imageFrame.find("img").first().attr("src"), this.getTags());
	}
};

// Event Methods

Editor.prototype.onTemplateLoad_Editor = function ( template ) {
	if ( template ) {
		this.el.empty();
		this.el.html(template({strings:this.i18.strings}));
		this.bindListeners();
	}
};

Editor.prototype.onClick_ImageFrame = function ( e ) {
	if ( !this.disabled ) {
		this.fileInput.trigger("click");	
	}
};

Editor.prototype.onClick_SubmitButton = function ( e ) {
	if ( !this.finished ) {
		this.submit();
	}
};

Editor.prototype.onClick_CancelButton = function ( e ) {
	if ( !this.disabled || this.disabled && this.finished ) {
		this.el.remove();
	}
}

Editor.prototype.onChange_FileInput = function ( e ) {
	var reader = new FileReader();
	reader.onload = (function ( e ) {
		this.setWallpaperSelected(e.target.result);
	}).bind(this);
	reader.readAsDataURL(e.target.files[0]);
};

Editor.prototype.onApi_AddImage = function ( response ) {
	this.enable();
	
	if ( response.status == 0 ) {
		this.finished = true;
		this.disabled = true;
		this.submitButton.text(this.i18.strings["editor"]["submit-completed"]);
		this.submitButton.addClass("green");
		this.submitButton.addClass("disabled");
		this.tagsInput.attr("disabled", true);
		this.tagsInput.addClass("disabled");
	}
	else {
		this.submitButton.addClass("red");
		this.submitButton.text(this.i18.strings["editor"]["submit-error"]);
	}
}

module.exports.Class = Editor;