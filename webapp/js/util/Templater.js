require("./../lib/jquery");
var _ = require("./../lib/underscore");

function Templater  () {
	this.map = {};
}

Templater.prototype.load = function ( template, callback, reload ) {
	if ( !reload && this.map[template] ) {
		callback(this.map[template]);
		return;
	}

	var options = {
		url: template
	};
	var t = $.ajax(options);
	t.done((function(data){
		this.map[template] = _.template(data);
		callback(this.map[template]);
	}).bind(this));
	t.fail((function(){
		callback(null);
	}).bind(this));
};

module.exports.Class = Templater;