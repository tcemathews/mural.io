module.exports = {
	"download":"Download",
	"close":"Close",
	"tag-list":"Tags",
	"continue-searching":"Continue searching...",
	"no-results":"Nothing found.",
	"no-more-results":"You have found everything for the current query.",
	"problem-getting-results":"There was a problem with retrieving the results. Please try again later.",
	"editor": {
		"placeholder-label":"Select Wallpaper",
		"add-button-text":"Submit",
		"close-button-text":"Close",
		"request-password":"Please provide the secret password.",
		"submit-completed":"Completed",
		"submit-error":"Try Again"
	}
};