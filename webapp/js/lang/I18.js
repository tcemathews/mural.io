function I18 () {
	this.strings = {};
}

I18.prototype.setLanguage = function ( value ) {
	this.strings = this.getLanguageStrings(value);
};

I18.prototype.getLanguageStrings = function ( value ) {
	return require("./english");
};

module.exports.Class = I18;