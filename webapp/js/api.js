require("./lib/jquery");

module.exports = {
	url: "/",
	translateResponse: function (data) {
		return JSON.parse(data);
	},
	translateError: function () {
		return {
			status: 1,
			message: "Request failed.",
			data: null
		}
	},
	call: function ( url, data, callback ) {
		var options = {};
		options.url = this.url + url;
		options.type = "POST";
		options.data = JSON.stringify(data);
		var t = $.ajax(options);
		t.done((function (data) {
			callback(this.translateResponse(data));
		}).bind(this));
		t.fail((function () {
			callback(this.translateError());
		}).bind(this));
	},
	getLatestImages: function (callback, offset, limit) {
		this.call("api/list/latest", {data:{offset:offset, limit:limit}}, callback);
	},
	getTaggedImages: function (callback, includes, excludes, offset, limit) {
		this.call("api/list/tagged", {data:{includes:includes, excludes:excludes, offset:offset, limit:limit}}, callback);
	},
	getImageTags: function (callback, imageId) {
		this.call("api/list/image-tags", {data:{imageId: imageId}}, callback);
	},
	getPopularTags: function ( callback ) {
		this.call("api/list/popular-tags", {}, callback);
	},
	addImage: function ( callback, password, image, tags ) {
		console.log("addImage:", password, tags, image);
		this.call("api/add/image", {data:{password:password, tags:tags}, image:image}, callback);
	}
};