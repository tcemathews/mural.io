var mysql = require("mysql");

// Constants
var MESSAGES = {
	ERROR:"Database error occured.",
	OFFLINE:"Database is not avaliable.",
	INSUFFICENT_DATA:"Insufficent data provided."
};

function DataAccess ( config ) {
	this.pool = mysql.createPool(config);
}

DataAccess.prototype.query = function (statement, parameters, callback) {
	this.pool.getConnection(function(err, conn){
		if ( err ) {
			if ( err.code == 'ECONNREFUSED' ){
				callback(MESSAGES.OFFLINE, []);
			}
			else {
				callback(err, []);
			}
		}
		else {
			conn.query(statement, parameters, callback);
		}

		if ( conn ) {
			conn.release();
		}
	});
}

DataAccess.prototype.fixQueryItems = function ( query, items, replacement ) {
	var arr = [];

	replacement = replacement ? replacement : "?";

	for ( var i in items ) {
		arr.push(replacement);
	}

	return query.replace(/_\?_/g, arr.join(", "));
}

// Retrieve Methods

DataAccess.prototype.getLatestFiles = function ( callback, offset, limit ) {
	this.query("SELECT id, filename FROM files ORDER BY date_created DESC LIMIT ?,?", [parseInt(offset), parseInt(limit)], callback)
};

DataAccess.prototype.getFilesByTags = function ( callback, includes, excludes, offset, limit ) {
	if ( includes.length <= 0 ) {
		callback(MESSAGES.INSUFFICENT_DATA, []);
		return;
	}
	
	var items = includes.concat();
	var query = this.fixQueryItems('SELECT DISTINCT t1.id, t1.filename FROM files t1 LEFT JOIN crossref_files_tags cr ON t1.id = cr.id_file WHERE cr.id_tag IN (SELECT id FROM tags WHERE name IN ( _?_ ))', includes);

	if ( excludes.length > 0 ) {
		query += this.fixQueryItems('AND t1.id NOT IN (SELECT DISTINCT t1.id FROM files t1 LEFT JOIN crossref_files_tags cr ON t1.id = cr.id_file WHERE cr.id_tag IN (SELECT id FROM tags WHERE name IN ( _?_ )))', excludes);
		items = items.concat(excludes);
	}

	query += "ORDER BY t1.date_created DESC LIMIT ?,?";
	items.push(offset, limit);

	this.query(query, items, callback);
};

DataAccess.prototype.getTagsForFile = function ( callback, imageId ) {
	this.query("SELECT t1.id, t1.name FROM tags AS t1 JOIN crossref_files_tags AS t2 WHERE t1.id = t2.id_tag AND t2.id_file = ?", [imageId], callback);
};

DataAccess.prototype.getPopularFileTags = function ( callback ) {
	this.query("SELECT id, name FROM tags ORDER BY hits DESC LIMIT 0, 50", [], callback);
}

DataAccess.prototype.addFileWithTags = function ( callback, filename, tags ) {
	this.query("INSERT INTO files (filename, date_created) VALUES (?, now())", [filename], (function (err, result) {
		if ( err ) {
			callback(err, []);
		}
		else {
			// Add tags to database.
			var imageId = result.insertId;
			var queryA = this.fixQueryItems("INSERT INTO tags (name) VALUES _?_ ON DUPLICATE KEY UPDATE hits=hits+1", tags, "(?)");
			this.query(queryA, tags, (function(err, results){
				if ( err ) {
					callback(err, []);
				}
				else {
					// Add file and tag references.
					var queryB = this.fixQueryItems("INSERT INTO crossref_files_tags (id_tag, id_file) SELECT id AS id_tag, ? AS id_file FROM tags WHERE name IN ( _?_ );", tags);
					var items = tags.concat();
					items.unshift(imageId);
					this.query(queryB, items, callback);
				}
			}).bind(this));
		}
	}).bind(this));
}

DataAccess.prototype.updateTagHits = function ( callback, tags ) {
	this.query(this.fixQueryItems("UPDATE tags SET hits=hits+1 WHERE name IN ( _?_ )", tags), tags, callback);
}

exports.Class = DataAccess;