var gm = require("gm").subClass({imageMagick:true});

function ImageProcessor () {
	
}

ImageProcessor.prototype.createThumbnail = function (size, imageUri, destinationUri, callback) {
	gm(imageUri).resize(size, size).noProfile().write(destinationUri, callback);
}

exports.Class = ImageProcessor;