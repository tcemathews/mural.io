var fs = require("fs");
var rimraf = require("rimraf");

var XServer = require("./http/XServer").Class;
var RequestDirective = require("./http/RequestDirective").Class;
var DataAccess = require("./data/DataAccess").Class;
var ApiResponse = require("./scripts/ApiResponse").Class;
var FileResponse = require("./scripts/FileResponse").Class;
var IndexResponse = require("./scripts/IndexResponse").Class;
var ImageProcessor = require("./image/ImageProcessor").Class;

function getStartOptions () {
	var options = {};

	for ( var i in process.argv ) {
		var exploded = process.argv[i].split("=");
		options[exploded[0]] = exploded.length > 1 ? exploded[1] : true;
	}

	return options;
}

function getConfiguration ( location ) {
	var contents = null;

	try {
		contents = fs.readFileSync(location, {encoding:"utf8"});	
	}
	catch ( ex ) {
		console.log(ex);
	}

	return contents ? JSON.parse(contents) : null;
}

function isValidConfiguration ( config ) {
	if ( !config || 
		!config.http.port || 
		!config.database || 
		!config.database.host || 
		!config.database.user ||
		!config.database.pass ||
		!config.database.schema ) {
		return false;
	}

	return true;
}

function generateThumbnails ( thumbnailSize, directories ) {
	// Empty the directory.
	rimraf(directories.thumbnails, function() {
		fs.mkdirSync(directories.thumbnails);	

		// Generate the thumbnails for each.
		var ip = new ImageProcessor();
		var files = fs.readdirSync(directories.images);
		var i = 0;
		
		var callback = function ( err ) {
			if ( err ) {
				console.log("Could not create thumbnail.", err);
			}
			doit();
		};
		var doit = function () {
			i++;
			if ( i < files.length ) {
				var file = files[i];
				console.log("Creating thumbnail for", file);
				ip.createThumbnail(thumbnailSize, directories.images + file, directories.thumbnails + file, callback);
			}
		}
		doit();
	});
}

function start () {
	var options = getStartOptions();
	var config = getConfiguration(options.config);

	if ( options.generatethumbnails ) {
		generateThumbnails(config.thumbnailSize, config.directories);
	}
	else if ( isValidConfiguration(config) ) {
		var ir = new IndexResponse();
		var xs = new XServer();
		var da = new DataAccess({
				host:config.database.host, 
				user:config.database.user, 
				password:config.database.pass, 
				database:config.database.schema});
		xs.start(config.http.port, function(directive) {
			var directories = config.directories;
			var response = null;

			switch ( directive.action ) {
				case "favicon.ico":
					break;
				case "api":
					response = new ApiResponse(directive, da, config);
					break;
				case "image":
					response = new FileResponse(directive, directories.images);
					break;
				case "thumbnail":
					response = new FileResponse(directive, directories.thumbnails);
					break;
				case "asset":
					response = new FileResponse(directive, directories.assets);
					break;
				default:
					ir.init(directive);
					response = ir;
					break;
			}

			return response;
		});
	}
	else {
		console.log("Configuration invalid.");
	}
}

start();