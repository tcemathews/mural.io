// Class Imports
var HttpResponseScript = require("../http/HttpResponseScript").Class;

// Imports
var mime = require("mime");
var fs = require("fs");

// Class Definition
function FileResponse ( directive, basePath ) {
	this.init(directive);
	this.path = this.createPath(directive.parameters);
	this.basePath = basePath;
}

// Inheritence
FileResponse.prototype.init = HttpResponseScript.prototype.init;
FileResponse.prototype.hasExceededMaxBytes = HttpResponseScript.prototype.hasExceededMaxBytes;
FileResponse.prototype.setStatus = HttpResponseScript.prototype.setStatus;
FileResponse.prototype.setContentType = HttpResponseScript.prototype.setContentType;
FileResponse.prototype.setData = HttpResponseScript.prototype.setData;
FileResponse.prototype.resultToNotFound = HttpResponseScript.prototype.resultToNotFound;
FileResponse.prototype.resultToRedirect = HttpResponseScript.prototype.resultToRedirect;
FileResponse.prototype.resultToMaxSizeExceeded = HttpResponseScript.prototype.resultToMaxSizeExceeded;
FileResponse.prototype.update = HttpResponseScript.prototype.update;

// Class Methods
FileResponse.prototype.createPath = function ( arr ) {
	var path = "";
	var count = arr.length;
	for ( var i = 0; i < count; i++ ) {
		if ( i != 0 ) {
			path += "/";
		}
		path += arr[i];
	}
	return path;
}

FileResponse.prototype.execute = function ( callback ) {
	var path = this.basePath + this.path;
	fs.exists(path, (function(exists) {
		if ( exists ) {
			fs.stat(path, (function(err, stats) {
				if ( stats.isFile() ) {
					this.setContentType(path);
					fs.readFile(path, (function ( err, data ) {
						this.setStatus(200);
						this.setData(data, "binary");
						HttpResponseScript.prototype.execute.call(this, callback);
					}).bind(this));
				}
				else {
					this.resultToNotFound("Requested file is not valid for return.");
					HttpResponseScript.prototype.execute.call(this, callback);
				}
			}).bind(this));
		}
		else {
			this.resultToNotFound("File does not exist");
			HttpResponseScript.prototype.execute.call(this, callback);
		}
	}).bind(this));
}

// Exports
exports.Class = FileResponse;