// Imports
// Class Imports
var HttpResponseScript = require("../http/HttpResponseScript").Class;
var HtmlNode = require("../http/HtmlNode").Class;

// Class Definition
function IndexResponse ( ) {
	this.html = new HtmlNode("html");
	this.head = new HtmlNode("head");
	this.body = new HtmlNode("body");
	this.html.addChildren(this.head, this.body);
	
	var utf8 = new HtmlNode("meta");
	utf8.setAttribute("charset", "utf-8");
	
	var viewport = new HtmlNode("meta");
	viewport.setAttribute("name", "viewport");
	viewport.setAttribute("content", "width=device-width, user-scalable=no");

	var title = new HtmlNode("title");
	title.addChildren("mural.io");

	var styles = new HtmlNode("link");
	styles.setAttribute("rel", "stylesheet");
	styles.setAttribute("href", "/asset/css/styles.css");

	var scripts = new HtmlNode("script");
	scripts.setAttribute("type", "text/javascript");
	scripts.setAttribute("src", "/asset/js/app.js");

	this.head.addChildren(utf8, viewport, title, styles, scripts);
}

// Inheritence
IndexResponse.prototype.init = HttpResponseScript.prototype.init;
IndexResponse.prototype.hasExceededMaxBytes = HttpResponseScript.prototype.hasExceededMaxBytes;
IndexResponse.prototype.setStatus = HttpResponseScript.prototype.setStatus;
IndexResponse.prototype.setContentType = HttpResponseScript.prototype.setContentType;
IndexResponse.prototype.setData = HttpResponseScript.prototype.setData;
IndexResponse.prototype.resultToNotFound = HttpResponseScript.prototype.resultToNotFound;
IndexResponse.prototype.resultToRedirect = HttpResponseScript.prototype.resultToRedirect;
IndexResponse.prototype.resultToMaxSizeExceeded = HttpResponseScript.prototype.resultToMaxSizeExceeded;
IndexResponse.prototype.update = HttpResponseScript.prototype.update;

// Class Methods
IndexResponse.prototype.execute = function ( callback ) {
	this.setStatus(200);
	this.setContentType("html");
	this.setData("<!doctype html>" + this.html.getHtml());
	HttpResponseScript.prototype.execute.call(this, callback);
}

// Exports
exports.Class = IndexResponse;