// Imports
var fs = require("fs");
var mime = require("mime");
var gm = require("gm").subClass({imageMagick:true});

// Class Imports
var HttpResponseScript = require("../http/HttpResponseScript").Class;
var DataAccess = require("../data/DataAccess").Class;

// Constants
var STATUS = {
	SUCCESS:0,
	UNKNOWN:1,
	ERROR:2,
	DATA_MISSING:3
};
var MESSAGE = {
	SUCCESS:"",
	UNKNOWN:"Unknown response has occured. It could be an error.",
	ERROR:"A general error has occured preventing a return of correct data.",
	DATA_MISSING:"Required data for the api method called is missing.",
	NOT_EXIST:"The requested method does not exist.",
	CANNOT_PROCESS:"The image provided could not be processed.",
	CANNOT_CREATE_THUMBNAIL:"Could not create a thumbnail for the image provided."
};

// Class Definition
function ApiResponse ( directive, dataAccess, config ) {
	this.init(directive);
	this.directive = directive;
	this.config = config;
	this.maxBytes = this.config.maxRequestSize;
	this.map = {
		"list":{
			"latest":this.retrieveLatest,
			"tagged":this.retrieveTagged,
			"image-tags":this.retrieveImageTags,
			"popular-tags":this.retrievePopularTags
		},
		"add":{
			"image":this.addImage
		}
	};
	this.dataAccess = dataAccess;
}

// Inheritence
ApiResponse.prototype.init = HttpResponseScript.prototype.init;
ApiResponse.prototype.hasExceededMaxBytes = HttpResponseScript.prototype.hasExceededMaxBytes;
ApiResponse.prototype.setStatus = HttpResponseScript.prototype.setStatus;
ApiResponse.prototype.setContentType = HttpResponseScript.prototype.setContentType;
ApiResponse.prototype.setData = HttpResponseScript.prototype.setData;
ApiResponse.prototype.resultToNotFound = HttpResponseScript.prototype.resultToNotFound;
ApiResponse.prototype.resultToRedirect = HttpResponseScript.prototype.resultToRedirect;
ApiResponse.prototype.resultToMaxSizeExceeded = HttpResponseScript.prototype.resultToMaxSizeExceeded;
ApiResponse.prototype.update = HttpResponseScript.prototype.update;

// Class Methods
ApiResponse.prototype.execute = function ( callback ) {
	var apiCall = this.getApiCall(this.directive.parameters);
	if ( apiCall != null ) {
		apiCall.call(this, (function () {
			HttpResponseScript.prototype.execute.call(this, callback);	
		}).bind(this));
	}
	else {
		this.setStatus(404);
		this.setContentType("txt");
		this.setData(MESSAGE.NOT_EXIST);
		HttpResponseScript.prototype.execute.call(this, callback);	
	}
}

ApiResponse.prototype.resultToApiResponse = function ( jsonResponse ) {
	this.setStatus(200);
	this.setContentType("txt");
	this.setData(jsonResponse);
}

ApiResponse.prototype.resultToApiResponseError = function ( status, message ) {
	this.resultToApiResponse(this.formatResponseJson(status, message, null));
}

ApiResponse.prototype.getApiCall = function ( pieces ) {
	var obj = this.map;
	var count = pieces.length;
	for ( var i = 0; i < count; i++ ) {
		var piece = pieces[i];
		if ( obj[piece] == undefined || obj[piece] == null ) {
			return null;
		}
		else {
			obj = obj[piece];
		}
	}
	return obj;
}

ApiResponse.prototype.getRequestBodyObject = function () {
	var body = null;

	try {
		body = JSON.parse(this.directive.body);	
	}
	catch ( ex ) {
		body = {};
		console.log("Error parsing body to json.", ex);
	}

	return body;
}

ApiResponse.prototype.formatResponseJson = function ( status, message, data ) {
	var s = parseInt(status);
	var m = message !== undefined ? String(message) : MESSAGE.UNKNOWN;
	var obj = {
		"status":s,
		"message":m,
		"data":data
	};
	return JSON.stringify(obj);
}

// Helper Methods

ApiResponse.prototype.padTimeInteger = function ( value, padding ) {
	value = parseInt(value);
	padding = parseInt(padding) || 0;

	var pads = "";
	for ( var i = 0; i < padding; i++ ) {
		pads += "0";
	}

	return (pads + value).slice(-1 - pads.length);
}

ApiResponse.prototype.getDateString = function () {
	var d = new Date();
	return d.getFullYear() + this.padTimeInteger(d.getMonth() + 1, 1) + this.padTimeInteger(d.getDate(), 1) + this.padTimeInteger(d.getHours(), 1) + this.padTimeInteger(d.getMinutes(), 1) + this.padTimeInteger(d.getMilliseconds(), 2);
}

ApiResponse.prototype.saveBase64Image = function ( imageStr, location, callback ) {
	var name = this.getDateString();
	var explode = imageStr.split(",");
	var meta = explode[0];
	var raw = explode[1];
	var encoding = meta.substr(meta.indexOf(";")+1);
	var ext = mime.extension(meta.substring(meta.indexOf("data:")+5, meta.indexOf(";")));
	var filename = name + "." + ext;

	try {
		fs.writeFile(location + filename, raw, encoding, function(err){
			callback(null, filename);
		});
	}
	catch ( e ) {
		callback(e);
	}
}

ApiResponse.prototype.processTemporaryImage = function ( filename, location, thumbnailsLocation, imagesLocation, callback ) {
	// TODO make sure image is a correct size.
	var uri = location + filename;
	gm(uri).resize(1024, 1024).noProfile().write(thumbnailsLocation + filename, function (err) {
		if ( err ) {
			callback(MESSAGE.CANNOT_CREATE_THUMBNAIL);
		}
		else {
			fs.rename(uri, imagesLocation + filename, function(err) {
				if ( err ) {
					callback(MESSAGE.CANNOT_PROCESS);
				}
				else {
					callback();
				}
			})
		}
	});
}

// Retrieve Methods

ApiResponse.prototype.retrieveLatest = function ( callback ) {
	var data = this.getRequestBodyObject().data;
	var offset = data && data.offset ? data.offset : 0;
	var limit = data && data.limit ? data.limit : 50;
	this.dataAccess.getLatestFiles((function ( err, rows ) {
		if ( err ) {
			this.resultToApiResponseError(STATUS.ERROR, err);
		}
		else {
			this.resultToApiResponse(this.formatResponseJson(STATUS.SUCCESS, MESSAGE.SUCCESS, rows));
		}
		callback();
	}).bind(this), offset, limit);
}

ApiResponse.prototype.retrieveTagged = function ( callback ) {
	var data = this.getRequestBodyObject().data;
	if ( data && data.includes && data.excludes ) {
		this.dataAccess.getFilesByTags((function (err, rows) {
			if ( err ) {
				this.resultToApiResponseError(STATUS.ERROR, err);
			}
			else {
				this.resultToApiResponse(this.formatResponseJson(STATUS.SUCCESS, MESSAGE.SUCCESS, rows));
				this.dataAccess.updateTagHits(function(err, result) { }, data.includes);
			}
			callback();
		}).bind(this), data.includes, data.excludes, data.offset || 0, data.limit || 50);
	}
	else {
		this.resultToApiResponseError(STATUS.DATA_MISSING, MESSAGE.DATA_MISSING);
		callback();
	}
}

ApiResponse.prototype.retrieveImageTags = function ( callback ) {
	var data = this.getRequestBodyObject().data;
	if ( data && data.imageId ) {
		this.dataAccess.getTagsForFile((function (err, rows) {
			if ( err ) {
				this.resultToApiResponseError(STATUS.ERROR, err);
			}
			else {
				this.resultToApiResponse(this.formatResponseJson(STATUS.SUCCESS, MESSAGE.SUCCESS, rows));
			}
			callback();
		}).bind(this), data.imageId);
	}
	else {
		this.resultToApiResponseError(STATUS.DATA_MISSING, MESSAGE.DATA_MISSING);
		callback();
	}
}

ApiResponse.prototype.retrievePopularTags = function ( callback ){
	this.dataAccess.getPopularFileTags((function(err, rows){
		if ( err ) {
			this.resultToApiResponseError(STATUS.ERROR, err);
		}
		else {
			this.resultToApiResponse(this.formatResponseJson(STATUS.SUCCESS, MESSAGE.SUCCESS, rows));
		}
		callback();
	}).bind(this));
}

ApiResponse.prototype.addImage = function ( callback ) {
	var body = this.getRequestBodyObject();
	var data = body.data;
	var image = body.image;

	// Remove data from memory as soon as possible.
	this.directive.body = null;
	body = null;

	if ( data && data.tags && image && data.password == this.config.apiPassword ) {
		var tempdir = this.config.directories.temp;
		var thumbnailsLocation = this.config.directories.thumbnails;
		var imagesLocation = this.config.directories.images;

		// Save the image to a temporary location.
		this.saveBase64Image(image, tempdir, (function(err, filename){
			image = null; // Remove final reference.

			if ( err ) {
				this.resultToApiResponseError(STATUS.ERROR, MESSAGE.CANNOT_PROCESS);
				callback();
			}
			else {
				// Process the image for use.
				this.processTemporaryImage(filename, tempdir, thumbnailsLocation, imagesLocation, (function(err){
					if ( err ) {
						this.resultToApiResponseError(STATUS.ERROR, err);
						callback();
					}
					else {
						// Make record in database.
						this.dataAccess.addFileWithTags((function (err, results) {
							if ( err ) {
								this.resultToApiResponseError(STATUS.ERROR, err);
							}
							else {
								this.resultToApiResponse(this.formatResponseJson(STATUS.SUCCESS, MESSAGE.SUCCESS, []));
							}
							callback();
						}).bind(this), filename, data.tags);
					}
				}).bind(this));
			}
		}).bind(this));
	}
	else {
		this.resultToApiResponseError(STATUS.DATA_MISSING, MESSAGE.DATA_MISSING);
		callback();
	}
}

// Exports
exports.Class = ApiResponse;