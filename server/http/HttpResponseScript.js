var mime = require("mime");

function HttpResponseScript ( directive ) {
	this.init(directive);
}

HttpResponseScript.prototype.init = function ( directive ) {
	this.directive = directive;
	this.result = {
		status:200
		,head:{"Content-Type":mime.lookup("txt")}
		,data:""
		,encoding:"utf8"
	};
	this.maxBytes = 100;
}

HttpResponseScript.prototype.hasExceededMaxBytes = function () {
	return this.directive.body.length > this.maxBytes;
}

HttpResponseScript.prototype.setStatus = function ( status ) {
	this.result.status = parseInt(status);
}

HttpResponseScript.prototype.setContentType = function ( value ) {
	this.result.head["Content-Type"] = mime.lookup(value);
}

HttpResponseScript.prototype.setData = function ( data, encoding ) {
	this.result.data = data != undefined ? data : "";
	this.result.encoding = encoding != undefined ? encoding : "utf8";
}

HttpResponseScript.prototype.resultToNotFound = function ( message ) {
	this.setStatus(404);
	this.setContentType("txt");
	this.setData("Error - Not Found: " + message);
}

HttpResponseScript.prototype.resultToRedirect = function ( location ) {
	this.setStatus(302);
	this.result.head = { "Location":location };
	this.setData(null, null);
}

HttpResponseScript.prototype.resultToMaxSizeExceeded = function () {
	this.setStatus(413);
	this.setContentType("txt");
	this.setData("You have reached the maximum transfer size: " + this.maxBytes + " bytes.");
}

HttpResponseScript.prototype.execute = function ( callback ) {
	callback(this.result);
}

HttpResponseScript.prototype.update = function () {
	if ( this.hasExceededMaxBytes() ) {
		this.resultToMaxSizeExceeded();
		return true;
	}
	return false;
}

exports.Class = HttpResponseScript;