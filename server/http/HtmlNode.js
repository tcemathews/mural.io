// Private Static Properties
var SELF_CLOSING_TAGS = ["area", "base", "br", "col", "command", "embed", "hr", "img", "input", "keygen", "link", "meta", "param", "source", "track", "wbr"];

// Class Definition
function HtmlNode ( tag ) {
	this.tag = tag;
	this.children = [];
	this.attributes = {};
}

// Class Methods
HtmlNode.prototype.isSelfClosing = function () {
	return SELF_CLOSING_TAGS.indexOf(this.tag.toLowerCase()) > -1;
}

HtmlNode.prototype.getHtml = function () {
	var selfClosing = this.isSelfClosing();
	
	var str = "<" + this.tag;
	
	for ( var prop in this.attributes ) {
		str += " " + String(prop);
		if ( this.attributes[prop] != null ) {
			str += "=\"" + this.getAttribute(prop) + "\"";
		}
	}

	if ( selfClosing ) {
		str += "/";
	}
	str += ">";

	var count = this.children.length;
	for ( var i = 0; i < count; i++ ) {
		var child = this.children[i];
		try {
			str += child.getHtml();
		}	
		catch ( e ) {
			str += String(child);
		}
	}

	if ( !selfClosing ) {
		str += "</" + this.tag + ">";
	}

	return str;
}

HtmlNode.prototype.addChildren = function ( ) {
	var count = arguments.length;
	for ( var i = 0; i < count; i++ ) {
		this.children.push(arguments[i]);
	}
}

HtmlNode.prototype.setAttribute = function ( attribute, value ) {
	if ( value === undefined ) {
		delete this.attributes[attribute];
	}
	else {
		this.attributes[attribute] = value;
	}
}

HtmlNode.prototype.getAttribute = function ( attribute ) {
	return this.attributes[attribute] instanceof Array ? this.attributes[attribute].join(" ") : String(this.attributes[attribute]);
}

HtmlNode.prototype.addAttributeValue = function ( attribute, value ) {
	if ( !(this.attributes[attribute] instanceof Array) ) {
		var oldValue = this.attributes[attribute];
		this.attributes[attribute] = [oldValue];
	}
	if ( value instanceof array ) {
		this.attributes[attribute] = this.attributes[attribute].concat(value);
	}
	else {
		this.attributes[attribute].push(value);
	}
}

HtmlNode.prototype.removeAttributeValue = function ( attribute, value ) {
	if ( this.attributes[attribute] instanceof Array ) {
		this.attributes[attribute].forEach(function ( item, index, arr ) {
			if ( item == value ) {
				arr.splice(index, 1);
			}
		});
	}
}

// Exports
exports.Class = HtmlNode;