// Imports
var http = require("http");

// Class Imports
var RequestDirective = require("./RequestDirective").Class;

// Class Definition
function XServer () {
	this.httpServer = null;
}

XServer.prototype.start = function ( port, requestScriptCallback ) {
	this.stop();
	this.httpServer = http.createServer((function ( request, response ) {
		var directive = new RequestDirective(request);
		var script = requestScriptCallback(directive);
		var onend = (function() {
			request.removeListener("end", onend);
			request.removeListener("data", ondata);

			if ( script ) {
				script.execute((function(result){
					this.finishResponse(response, result);
				}).bind(this));
			}
			else {
				response.end();
			}
		}).bind(this);
		var ondata = (function(chunk) {
			if ( script ) {
				// Update the directive body data.
				directive.body += chunk;

				// Update will tell us if we should end early or not.
				if ( script.update() ) {									
					request.removeListener("end", onend);
					request.removeListener("data", ondata);
					this.finishResponse(response, script.result);
				}
			}
		}).bind(this);

		request.on("end", onend);
		request.on("data", ondata);
	}).bind(this)).listen(port);
}

XServer.prototype.stop = function () {
	if ( this.httpServer != null ) {
		this.httpServer.close();
		this.httpServer = null;
	}
}

XServer.prototype.finishResponse = function ( response, result ) {
	response.writeHead(result.status, result.head);
	if ( result.data != null && result.encoding != null ) {
		response.write(result.data, result.encoding);
	}
	response.end();
}

// Exports
exports.Class = XServer;