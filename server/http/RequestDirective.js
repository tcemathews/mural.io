function RequestDirective ( request ) {
	this.url = request.url;
	this.action = "";
	this.parameters = [];
	this.body = "";

	// Parse the raw url to fill our action and parameters.
	var arr = request.url.split(/\//g);
	if ( arr.length > 1 ) {
		this.action = arr[1];
		this.parameters = arr.slice(2, arr.length);
	}
}

exports.Class = RequestDirective;