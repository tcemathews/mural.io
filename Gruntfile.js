module.exports = function (grunt) {
	grunt.initConfig({
		browserify: {
			debug: {
				files: {
					"./bin/assets/js/app.js": ["./webapp/js/**/*.js"]
				}
			},
			dist: {
				files: {
					"./bin/assets/js/app.js": ["./webapp/js/**/*.js"]
				}
			}
		},
		uglify: {
			default: {
				files:{
					"./bin/assets/js/app.js":"./bin/assets/js/app.js"
				}
			}
		},
		less: {
			debug: {
				options: {
					paths: ["./webapp/less/"],
					compress: false,
					yuicompress: false,
				},
				src: "./webapp/less/**/*.less",
				dest: "./bin/assets/css/styles.css"
			},
			dist: {
				options: {
					paths: ["./webapp/less/"],
					compress: true
				},
				src: "./webapp/less/**/*.less",
				dest: "./bin/assets/css/styles.css"
			}
		},
		copy: {
			default: {
				files: [
					{
						expand: true,
						flatten: true,
						src: ["./webapp/img/**"],
						dest: "./bin/assets/img/",
						filter: "isFile"
					},
					{
						expand: true,
						flatten: true,
						src: ["./webapp/templates/**"],
						dest: "./bin/assets/template/",
						filter: "isFile"
					}
				]
			}
		},
		watch: {
			default: {
				files: ["./webapp/less/**/*.less", "./webapp/js/**/*.js", "./webapp/templates/**/*"],
				tasks: ["less:debug", "browserify:debug", "copy"]
			}
		}
	});

	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-browserify");

	grunt.registerTask("debug", ["less:debug", "browserify:debug", "copy"]);
	grunt.registerTask("dist", ["less:dist", "browserify:dist", "copy", "uglify"]);
}