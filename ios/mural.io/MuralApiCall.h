//
//  MuralApiCall.h
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventDispatcher.h"

@interface MuralApiCall : NSObject <NSURLConnectionDataDelegate> {
    NSMutableData* receivedData;
    NSURL* url;
    NSData* json;
    EventDispatcher* on;
    NSDictionary* result;
}

@property (readonly) EventDispatcher* on;
@property (readonly) NSDictionary* result;

- (id)init:(NSString*)url withData:(NSDictionary*)data;
- (void)send;

@end
