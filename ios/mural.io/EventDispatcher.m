//
//  EventDispatcher.m
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "EventDispatcher.h"

@implementation EventDispatcher

- (id)init {
    self = [super init];
    if ( self ) {
        types = [[NSMutableDictionary alloc] init];
        temps = [[NSMutableDictionary alloc] init];
        firing = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (int)getObjectIndex:(id)object inNestedArray:(NSArray*)arr {
    int count = (int)[arr count];
    for ( int i = 0; i < count; i++ ){
        if ( object == arr[i][0] ) {
            return i;
        }
    }
    return -1;
}

- (void)dispatch:(Event*)event {
    // Ensure double firing is avoided.
    bool isFiring = [[firing valueForKey:event.type] boolValue];
    if ( isFiring ) {
        return;
    } 
    
    // Fire all effected objects.
    [firing setValue:[NSNumber numberWithBool:true] forKey:event.type];
    NSMutableArray* arr = [types objectForKey:event.type];
    int count = (int)[arr count];
    
    for ( int i = 0; i < count; i++ ) {
        id listener = arr[i][0];
        NSString* selectorName = arr[i][1];
        SEL sel = sel_registerName([selectorName UTF8String]);
        IMP imp = [listener methodForSelector:sel];
        void (*func)(id, SEL, Event* evt) = (void*)imp;
        func(listener, sel, event);
    }
    
    // Add any objects that were added from the effects of the fire that occured before.
    NSMutableArray* arrB = [temps objectForKey:event.type];
    if ( arrB != nil && [arrB count] > 0 ) {
        [arr addObjectsFromArray:arrB];
        [temps removeObjectForKey:event.type];
    }
    
    // Enable firing again.
    [firing setValue:[NSNumber numberWithBool:false] forKey:event.type];
}

- (void)addListener:(id)object withSelector:(SEL)selector forType:(NSString*)type {
    // If firing add to the temporary storage for the fire after the current.
    if ( [[firing valueForKey:type] boolValue] ) {
        NSMutableArray* arr = [temps objectForKey:type];
        if ( arr == nil ) {
            arr = [[NSMutableArray alloc] init];
            [temps setObject:arr forKey:type];
        }
        [arr addObject:[[NSArray alloc] initWithObjects:object, NSStringFromSelector(selector), nil]];
    }
    // If not firing add to the immediate storage for next fire.
    else {
        NSMutableArray* arr = [types objectForKey:type];
        if ( arr == nil ) {
            arr = [[NSMutableArray alloc] init];
            [types setObject:arr forKey:type];
        }
        [arr addObject:[[NSArray alloc] initWithObjects:object, NSStringFromSelector(selector), nil]];
    }
}

- (void)removeListener:(id)object forType:(NSString *)type {
    int index = 0;
    
    // Remove from types.
    NSMutableArray* arr = [types objectForKey:type];
    if ( arr != nil ) {
        while ( index > -1 ) {
            index = [self getObjectIndex:object inNestedArray:arr];
            if ( index > -1 ) {
                [arr removeObjectAtIndex:index];
            }
        }
    }
    
    // Remove from temps.
    arr = nil;
    arr = [temps objectForKey:type];
    index = 0;
    if ( arr != nil ) {
        while ( index > -1 ) {
            index = [self getObjectIndex:object inNestedArray:arr];
            if ( index > -1 ) {
                [arr removeObjectAtIndex:index];
            }
        }
    }
}

@end
