//
//  Event.m
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "Event.h"

@implementation Event

@synthesize target, type, data;

- (id)initWithType:(NSString *)typ withTarget:(id)trgt {
    self = [super init];
    if ( self ) {
        type = typ;
        target = trgt;
        data = nil;
    }
    return self;
}

- (id)initWithType:(NSString *)typ withTarget:(id)trgt withData:(NSArray *)dt {
    self = [self initWithType:typ withTarget:trgt];
    if ( self ) {
        data = dt;
    }
    return self;
}

@end
