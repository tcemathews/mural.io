//
//  ImageListCell.m
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "ImageListCell.h"

@implementation ImageListCell

- (id)initWithReuseIdentifier:(NSString*)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, self.frame.size.width - 40, 100)];
        [imageView setBackgroundColor:[UIColor lightGrayColor]];
        [self addSubview:imageView];
    }
    return self;
}

- (void)setImageToDisplay:(UIImage *)image {
    [imageView setImage:image];
    int width = image.size.width;
    int height = image.size.height;
    height = (int)(height * (imageView.frame.size.width / width));
    if ( height > 0.0f ) {
        imageView.frame = CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y, imageView.frame.size.width, height);
    }
}

+ (float)getHeightWithImage:(UIImage*)image {
    int height = (int)(image.size.height * (280 / image.size.width));
    if ( height == 0.0f ) {
        height = 100;
    }
    return 20.0f + height + 20.0f;
}

@end
