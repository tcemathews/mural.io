//
//  ImageListViewController.h
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDispatcher.h"

@class ImageListDataSource;

@interface ImageListViewController : UITableViewController{
    ImageListDataSource* datasource;
    EventDispatcher* on;
}

@property (readonly) ImageListDataSource* datasource;
@property (readonly) EventDispatcher* on;

- (id)initWithDataSource:(ImageListDataSource*)datasource;

@end
