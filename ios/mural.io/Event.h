//
//  Event.h
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject {
    id          target;
    NSString*   type;
    NSArray*    data;
}

@property (readonly) id         target;
@property (readonly) NSString*  type;
@property (readonly) NSArray*   data;

- (id)initWithType:(NSString*)type withTarget:(id)target;
- (id)initWithType:(NSString*)type withTarget:(id)target withData:(NSArray*)data;

@end
