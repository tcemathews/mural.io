//
//  ImageListDataSource.m
//  mural.io
//
//  Created by Thomas Mathews on 12/24/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "ImageListDataSource.h"
#import "ImageListCell.h"
#import "MuralApi.h"
#import "MuralApiCall.h"
#import "ImageCache.h"
#import "Event.h"

@implementation ImageListDataSource

@synthesize limit, offset, on, cache, uris, data;

- (id)initWithApi:(MuralApi*)apii andImageCache:(ImageCache*)imageCache {
    self = [super init];
    if ( self ) {
        api = apii;
        cache = imageCache;
        limit = 25;
        offset = 0;
        on = [[EventDispatcher alloc] init];
        uris = [[NSMutableArray alloc] init];
        
        [cache.on addListener:self withSelector:@selector(onImageLoaded:) forType:@"ImageCache:ImageLoaded"];
    }
    return self;
}

- (void)getLatest {
    [cache clear];
    MuralApiCall* call = [api getLatestStartingAt:offset counting:limit];
    [call.on addListener:self withSelector:@selector(onGetResults:) forType:@"MuralApiCall:DidFinish"];
    [call.on addListener:self withSelector:@selector(onError:) forType:@"MuralApiCall:DidError"];
    [call send];
}

- (void)getTagged:(NSArray *)tags {
    [cache clear];
    MuralApiCall* call = [api getTagged:tags startingAt:offset counting:limit];
    [call.on addListener:self withSelector:@selector(onGetResults:) forType:@"MuralApiCall:DidFinish"];
    [call.on addListener:self withSelector:@selector(onError:) forType:@"MuralApiCall:DidFail"];
    [call send];
}

- (void)getNextResults {
    
}

- (void)getPreviousResults {
    
}

- (void)parseData:(NSArray*)dta {
    [uris removeAllObjects];
    int count = (int)[dta count];
    for ( int i = 0; i < count; i++ ) {
        NSDictionary* dic = [dta objectAtIndex:i];
        NSString* url = [api.thumbnailUri stringByAppendingString:[dic valueForKey:@"filename"]];
        [uris addObject:url];
        [cache loadImage:url];
    }
    data = dta;
    [on dispatch:[[Event alloc] initWithType:@"ImageListDataSource:Updated" withTarget:self]];
}

#pragma mark - Event Methods

- (void)onImageLoaded:(Event*)event {
    [on dispatch:[[Event alloc] initWithType:@"ImageListDataSource:Updated" withTarget:self]];
}

- (void)onError:(Event*)event {
    NSLog(@"Did Error %@", event.data);
}

- (void)onGetResults:(Event*)event {
    [[event.target on] removeListener:self forType:@"MuralApiCall:DidFinish"];
    [[event.target on] removeListener:self forType:@"MuralApiCall:DidFail"];
    
    NSDictionary* dic = event.data[0];
    if ( [[dic valueForKey:@"status"] integerValue] == 0 ) {
        [self parseData:[dic objectForKey:@"data"]];
    }
    else {
        // TODO Handle api error. Show an alert maybe?
    }
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [uris count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"defaultcell";
    ImageListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ( cell == nil ) {
        cell = [[ImageListCell alloc] initWithReuseIdentifier:identifier];
    }
    
    [cell setImageToDisplay:[cache getImage:[uris objectAtIndex:indexPath.row]]];
    
    return cell;
}

@end
