//
//  ImageListDataSource.h
//  mural.io
//
//  Created by Thomas Mathews on 12/24/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventDispatcher.h"
#import "ImageCache.h"

@class MuralApi;

@interface ImageListDataSource : NSObject <UITableViewDataSource> {
    int limit;
    int offset;
    EventDispatcher* on;
    MuralApi* api;
    ImageCache* cache;
    NSArray* data;
    NSMutableArray* uris;
}

@property (readonly) int limit;
@property (readonly) int offset;
@property (readonly) EventDispatcher* on;
@property (readonly) ImageCache* cache;
@property (readonly) NSArray* uris;
@property (readonly) NSArray* data;

- (id)initWithApi:(MuralApi*)api andImageCache:(ImageCache*)imageCache;
- (void)getLatest;
- (void)getTagged:(NSArray*)tags;
- (void)getNextResults;
- (void)getPreviousResults;

@end
