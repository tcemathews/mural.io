//
//  MainViewController.h
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageCache;
@class MuralApi;
@class SearchViewController;
@class ImageListViewController;

@interface MainViewController : UIViewController {
    MuralApi* api;
    ImageCache* cache;
    SearchViewController* search;
    ImageListViewController* list;
}

@end
