//
//  ImageListViewController.m
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "ImageListViewController.h"
#import "ImageListDataSource.h"
#import "ImageListCell.h"
#import "Event.h"

@implementation ImageListViewController

@synthesize datasource, on;

- (id)initWithDataSource:(ImageListDataSource*)ds {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        datasource = ds;
        on = [[EventDispatcher alloc] init];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.tableView setDataSource:datasource];
        [datasource.on addListener:self withSelector:@selector(imageListDataSourceUpdated:) forType:@"ImageListDataSource:Updated"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Event Methods

- (void)imageListDataSourceUpdated:(Event*)event {
    [[self tableView] reloadData];
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIImage* image = [datasource.cache getImage:[datasource.uris objectAtIndex:indexPath.row]];
    return [ImageListCell getHeightWithImage:image];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [on dispatch:[[Event alloc] initWithType:@"ImageListViewController:SelectionMade" withTarget:self withData:[[NSArray alloc] initWithObjects:[datasource.data objectAtIndex:indexPath.row], nil]]];
}

@end
