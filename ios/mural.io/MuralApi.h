//
//  MuralApi.h
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MuralApiCall.h"

@interface MuralApi : NSObject {
    NSString* base;
    NSString* thumbnailUri;
    NSString* imageUri;
}

@property (readonly) NSString* thumbnailUri;
@property (readonly) NSString* imageUri;

- (MuralApiCall*)getLatestStartingAt:(uint)start counting:(uint)count;
- (MuralApiCall*)getTagged:(NSArray*)tags startingAt:(uint)start counting:(uint)count;
- (MuralApiCall*)getImageTags;
- (MuralApiCall*)getPopularTags;

@end
