//
//  ImageListCell.h
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageListCell : UITableViewCell {
    UIImageView* imageView;
}

- (id)initWithReuseIdentifier:(NSString*)reuseIdentifier;
- (void)setImageToDisplay:(UIImage*)image;
+ (float)getHeightWithImage:(UIImage*)image;

@end
