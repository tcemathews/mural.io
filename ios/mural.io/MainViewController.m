//
//  MainViewController.m
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "MainViewController.h"
#import "ImageCache.h"
#import "MuralApi.h"
#import "SearchViewController.h"
#import "ImageListViewController.h"
#import "ImageListDataSource.h"
#import "DisplayViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)init
{
    self = [super init];
    if (self) {
        api = [[MuralApi alloc] init];
        cache = [[ImageCache alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    search = [[SearchViewController alloc] initWithNibName:nil bundle:nil];
    [[search on] addListener:self withSelector:@selector(onSearchChanged:) forType:@"Search:Changed"];
    [search view].frame = CGRectOffset([search view].frame, 0, [UIApplication sharedApplication].statusBarFrame.size.height);
    [[self view] addSubview:[search view]];
    
    ImageListDataSource* ds = [[ImageListDataSource alloc] initWithApi:api andImageCache:cache];
    int y = [UIApplication sharedApplication].statusBarFrame.size.height + [search view].frame.size.height;
    list = [[ImageListViewController alloc] initWithDataSource:ds];
    [list view].frame = CGRectMake(0, y, self.view.frame.size.width, self.view.frame.size.height - y);
    [[list on] addListener:self withSelector:@selector(onListViewSelectionMade:) forType:@"ImageListViewController:SelectionMade"];
    [[self view] addSubview:[list view]];
    
    [ds getLatest];
}

#pragma mark - Methods

- (void)updateSearch {
    NSArray* tags = [search getTags];
    if ( [tags count] > 0 ) {
        [list.datasource getTagged:tags];
    }
    else {
        [list.datasource getLatest];
    }
}

#pragma mark - Event Methods

- (void)onSearchChanged:(Event*)event {
    [self updateSearch];
}

- (void)onListViewSelectionMade:(Event*)event {
    NSDictionary* dic = event.data[0];
    [list.view setUserInteractionEnabled:NO];
    DisplayViewController* dvc = [[DisplayViewController alloc] initWithApi:api withImageCache:cache];
    [dvc displayOverViewController:self forImageData:dic];
}

@end
