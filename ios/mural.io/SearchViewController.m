//
//  SearchViewController.m
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "SearchViewController.h"
#import "Event.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

@synthesize textfield, on;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currentText = @"";
        on = [[EventDispatcher alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Methods

- (NSArray*)getTags {
    NSMutableArray* arrm = [[NSMutableArray alloc] init];
    NSArray* arr = [currentText componentsSeparatedByString:@","];
    int count = (int)[arr count];
    for ( int i = 0; i < count; i++ ) {
        NSString* tag = [arr objectAtIndex:i];
        tag = [tag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ( ![tag isEqualToString:@""] ) {
            [arrm addObject:tag];
        }
    }
    return arrm;
}

#pragma mark - UITextFeildDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    NSString* text = textField.text;
    if ( ![text isEqualToString:currentText] ) {
        currentText = text;
        [on dispatch:[[Event alloc] initWithType:@"Search:Changed" withTarget:self withData:[[NSArray alloc] initWithObjects:currentText, nil]]];
    }
    
    return YES;
}

@end
