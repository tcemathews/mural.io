//
//  ImageCache.h
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventDispatcher.h"

@interface ImageCache : NSObject {
    NSMutableDictionary* images;
    NSOperationQueue* queue;
    EventDispatcher* on;
}

@property (readonly) EventDispatcher* on;

- (void)loadImage:(NSString*)url;
- (UIImage*)getImage:(NSString*)url;
- (void)clear;

@end
