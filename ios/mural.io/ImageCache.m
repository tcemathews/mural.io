//
//  ImageCache.m
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "ImageCache.h"

@implementation ImageCache

@synthesize on;

- (id)init {
    self = [super init];
    if ( self ) {
        images = [[NSMutableDictionary alloc] init];
        queue = [NSOperationQueue new];
        on = [[EventDispatcher alloc] init];
    }
    return self;
}

- (void)loadImage:(NSString *)url {
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(backgroundLoadImage:) object:url];
    [queue addOperation:op];
}

- (void)backgroundLoadImage:(NSString*)url {
    NSData* d = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage* image = [[UIImage alloc] initWithData:d];
    // TODO Make sure image loaded and does not give an error.
    [self performSelectorOnMainThread:@selector(backgroundLoadImageCompleted:) withObject:[NSArray arrayWithObjects:url, image, nil] waitUntilDone:NO];
}

- (void)backgroundLoadImageCompleted:(NSArray*)arr {
    [images setObject:[arr objectAtIndex:1] forKey:[arr objectAtIndex:0]];
    [on dispatch:[[Event alloc] initWithType:@"ImageCache:ImageLoaded" withTarget:self withData:arr]];
}

- (UIImage*)getImage:(NSString *)url {
    return [images objectForKey:url];
}

- (void)clear {
    [images removeAllObjects];
}

@end
