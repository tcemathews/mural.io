//
//  DisplayViewController.m
//  mural.io
//
//  Created by Thomas Mathews on 12/24/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "DisplayViewController.h"
#import "LFGlassView.h"
#import "ImageCache.h"
#import "MuralApi.h"

@implementation DisplayViewController

- (id)initWithApi:(MuralApi*)apii withImageCache:(ImageCache*)imageCache {
    self = [super init];
    if (self) {
        api = apii;
        cache = imageCache;
        [cache.on addListener:self withSelector:@selector(onImageLoaded:) forType:@"ImageCache:ImageLoaded"];
        
    }
    return self;
}

- (void)dealloc {
    [cache.on removeListener:self forType:@"ImageCache:ImageLoaded"];
}

- (void)loadView {
    LFGlassView* gv = [[LFGlassView alloc] init];
    gv.blurRadius = 8.0f;
    gv.layer.cornerRadius = 0.0f;
    gv.userInteractionEnabled = YES;
    self.view = gv;
    
    imageView = [[UIImageView alloc] init];
    imageView.backgroundColor = [UIColor redColor];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:imageView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [self.view addGestureRecognizer:sgr];
}

- (void)handleSwipe:(UISwipeGestureRecognizer*)recognizer {
    NSLog(@"Swipped %@", recognizer);
}

- (void)displayOverViewController:(UIViewController *)viewController forImageData:(NSDictionary *)dic {
    // TODO Check if not already displaying.
    
    [self.view setFrame:viewController.view.frame];
    [self.view setAlpha:0.0f];
    
    filename = [dic valueForKey:@"filename"];
    identity = [[dic valueForKey:@"id"] intValue];
    url = [api.imageUri stringByAppendingString:filename];
    loading = true;
    [cache loadImage:url];
    
    [viewController.view addSubview:self.view];
    [UIView animateWithDuration:1.0 animations:^{
        self.view.alpha = 1.0f;
    }];
    
    
    
}

- (void)onImageLoaded:(Event*)event {
    UIImage* image = [cache getImage:url];
    if ( image != nil && loading ) {
        [imageView setImage:image];
        [imageView setAlpha:0.0f];
        [imageView setFrame:self.view.frame];
        loading = false;
        
        [UIView animateWithDuration:1.0f animations:^{
            [imageView setAlpha:1.0f];
        }];
    }
}

@end
