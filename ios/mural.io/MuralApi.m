//
//  MuralApi.m
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "MuralApi.h"

@implementation MuralApi

@synthesize thumbnailUri, imageUri;

- (id)init {
    self = [super init];
    if ( self ) {
        NSString* domain = @"http://mural.io/";
        base = [domain stringByAppendingString:@"api/"];
        thumbnailUri = [domain stringByAppendingString:@"thumbnail/"];
        imageUri = [domain stringByAppendingString:@"image/"];
    }
    return self;
}

- (MuralApiCall*)getLatestStartingAt:(uint)start counting:(uint)count {
    NSArray* arr = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:start], [NSNumber numberWithInt:count], nil];
    NSArray* keys = [[NSArray alloc] initWithObjects:@"offset", @"limit", nil];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:arr forKeys:keys];
    NSDictionary* mstr = [[NSDictionary alloc] initWithObjects:[[NSArray alloc] initWithObjects:dic, nil] forKeys:[[NSArray alloc] initWithObjects:@"data", nil]];
    return [[MuralApiCall alloc] init:[base stringByAppendingString:@"list/latest"] withData:mstr];
}

- (MuralApiCall*)getTagged:(NSArray*)tags startingAt:(uint)start counting:(uint)count {
    NSArray* arr = [[NSArray alloc] initWithObjects:tags, [[NSArray alloc] init], [NSNumber numberWithInt:start], [NSNumber numberWithInt:count], nil];
    NSArray* keys = [[NSArray alloc] initWithObjects:@"includes", @"excludes", @"offset", @"limit", nil];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:arr forKeys:keys];
    NSDictionary* mstr = [[NSDictionary alloc] initWithObjects:[[NSArray alloc] initWithObjects:dic, nil] forKeys:[[NSArray alloc] initWithObjects:@"data", nil]];
    return [[MuralApiCall alloc] init:[base stringByAppendingString:@"list/tagged"] withData:mstr];
}

- (MuralApiCall*)getImageTags {
    return [[MuralApiCall alloc] init:[base stringByAppendingString:@"list/image-tags"] withData:[[NSDictionary alloc] init]];
}

- (MuralApiCall*)getPopularTags {
    return [[MuralApiCall alloc] init:[base stringByAppendingString:@"list/popular-tags"] withData:[[NSDictionary alloc] init]];
}

@end
