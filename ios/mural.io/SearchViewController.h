//
//  SearchViewController.h
//  mural.io
//
//  Created by Thomas Mathews on 12/22/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDispatcher.h"

@interface SearchViewController : UIViewController <UITextFieldDelegate> {
    NSString* currentText;
    UITextField* textfield;
    EventDispatcher* on;
}

@property IBOutlet UITextField* textfield;
@property (readonly) EventDispatcher* on;

- (NSArray*)getTags;

@end
