//
//  MuralApiCall.m
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import "MuralApiCall.h"

@implementation MuralApiCall

@synthesize on, result;

- (id)init:(NSString*)aurl withData:(NSDictionary *)data {
    self = [super init];
    if ( self ) {
        receivedData = [[NSMutableData alloc] init];
        url = [[NSURL alloc] initWithString:aurl];
        json = [NSJSONSerialization dataWithJSONObject:data options:0 error:nil];
        on = [[EventDispatcher alloc] init];
    }
    return self;
}

- (void)send {
    if ( json != nil ) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:json];
        NSURLConnection* conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [conn start];
    }
    else {
        [on dispatch:[[Event alloc] initWithType:@"MuralApiCall:DidFail" withTarget:self]];
    }
}

#pragma mark - NSURLConnectionDataDelegate Methods

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [on dispatch:[[Event alloc] initWithType:@"MuralApiCall:DidFail" withTarget:self withData:[[NSArray alloc] initWithObjects:error, nil]]];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* err = nil;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:receivedData options:0 error:&err];
    
    if ( err ) {
        result = nil;
        [on dispatch:[[Event alloc] initWithType:@"MuralApiCall:DidFail" withTarget:self withData:[[NSArray alloc] initWithObjects:err, nil]]];
    }
    else {
        result = dic;
        [on dispatch:[[Event alloc] initWithType:@"MuralApiCall:DidFinish" withTarget:self withData:[[NSArray alloc] initWithObjects:result, nil]]];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData appendData:data];
}

@end
