//
//  EventDispatcher.h
//  mural.io
//
//  Created by Thomas Mathews on 12/21/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"

@interface EventDispatcher : NSObject {
    NSMutableDictionary* types;
    NSMutableDictionary* temps;
    NSMutableDictionary* firing;
}

- (void)dispatch:(Event*)event;
- (void)addListener:(id)object withSelector:(SEL)selector forType:(NSString*)type;
- (void)removeListener:(id)object forType:(NSString*)type;

@end
