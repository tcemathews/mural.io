//
//  DisplayViewController.h
//  mural.io
//
//  Created by Thomas Mathews on 12/24/2013.
//  Copyright (c) 2013 Thomas Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MuralApi;
@class ImageCache;

@interface DisplayViewController : UIViewController {
    MuralApi*       api;
    ImageCache*     cache;
    UIImageView*    imageView;
    NSString*       url;
    NSString*       filename;
    int             identity;
    bool            loading;
    UISwipeGestureRecognizer * sgr;
}

- (id)initWithApi:(MuralApi*)api withImageCache:(ImageCache*)imageCache;
- (void)displayOverViewController:(UIViewController*)viewController forImageData:(NSDictionary*)dic;

@end
